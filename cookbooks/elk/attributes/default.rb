default["jenkins_setup"]["javaPackage"] = "jdk-8u60-linux-x64.rpm"
default["jenkins_setup"]["javaUrl"] = "http://download.oracle.com/otn-pub/java/jdk/8u60-b27"
default["myvalue"] = "localhost"
default["elastic"]["config"] = "/etc/elasticsearch/elasticsearch.yml"
