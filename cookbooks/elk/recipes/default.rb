#
# Cookbook Name:: elk
# Recipe:: default
#
# Copyright 2016, YOUR_COMPANY_NAME
#
# All rights reserved - Do Not Redistribute
#

execute "Download java rpm" do
  command "wget --no-cookies --no-check-certificate --header 'Cookie: gpw_e24=http%3A%2F%2Fwww.oracle.com%2F; oraclelicense=accept-securebackup-cookie' -P /tmp #{node["jenkins_setup"]["javaUrl"]}/#{node["jenkins_setup"]["javaPackage"]}"
 not_if { File.exist?("/tmp/#{node["jenkins_setup"]["javaPackage"]}")}
end

#Install rpm packege download to /tmp dir

rpm_package "#{node["jenkins_setup"]["javaPackage"]}" do
 source  "/tmp/#{node["jenkins_setup"]["javaPackage"]}"
 action :install
end

cookbook_file "/etc/pki/rpm-gpg/GPG-KEY-elasticsearch" do
 source "GPG-KEY-elasticsearch"
 owner "root"
 group "root"
 mode "0644"
end

cookbook_file "/etc/yum.repos.d/elasticsearch.repo" do
 source "elasticsearch.repo"
 owner "root" 
 group "root"
 mode "0644"
end

package "elasticsearch" do
 action :install
end

template "#{node["elastic"]["config"]}" do
 source "elasticsearch.yml.erb"
 owner "root"
 group "elasticsearch"
 mode "0755"
end

service "elasticsearch" do
 action [:enable,:restart]
end

cookbook_file "/etc/yum.repos.d/kibana.repo" do
 source "kibana.repo"
 owner "root"
 group "root"
 mode "0644"
end

package "kibana" do
 action :install
end

template "/opt/kibana/config/kibana.yml" do
 source "kibana.yml.erb"
 owner "root"
 group "root"
 mode "0664"
end


service "kibana" do
 action [:enable,:restart]
end

package "epel-release" do
 action :install
end


package "nginx" do
 action :install
end

package "httpd-tools" do
 action :install
end


cookbook_file "/etc/nginx/htpasswd.users" do
 source "htpasswd.users"
 owner "root"
 group "root"
 mode "0644"
end

template "/etc/nginx/nginx.conf" do
 source "nginx.conf.erb"
 owner "root"
 group "root"
 mode "0664"
end

template "/etc/nginx/conf.d/kibana.conf" do
 source "kibana.conf.erb"
 owner "root"
 group "root"
 mode "0664"
end


service "nginx" do
 action [:enable,:restart]
end

cookbook_file "/etc/yum.repos.d/logstash.repo" do
 source "logstash.repo"
 owner "root"
 group "root"
 mode "0644"
end

package "logstash" do
 action :install
end


template "/etc/pki/tls/openssl.cnf" do
 source "openssl.cnf.erb"
 owner "root"
 group "root"
 mode "0644"
end

cookbook_file "/etc/pki/tls/certs/logstash-forwarder.crt" do
 source "logstash-forwarder.crt"
 owner "root"
 group "root"
 mode "0644"
end

cookbook_file "/etc/pki/tls/private/logstash-forwarder.key" do
 source "logstash-forwarder.key"
 owner "root"
 group "root"
 mode "0644"
end

cookbook_file "/etc/logstash/conf.d/02-beats-input.conf" do
 source "02-beats-input.conf"
 owner "root"
 group "root"
 mode "0644"
end

cookbook_file "/etc/logstash/conf.d/10-syslog-filter.conf" do
 source "10-syslog-filter.conf"
 owner "root"
 group "root"
 mode "0644"
end

cookbook_file "/etc/logstash/conf.d/30-elasticsearch-output.conf" do
 source "30-elasticsearch-output.conf"
 owner "root"
 group "root"
 mode "0644"
end

service "logstash" do
 action [:enable,:restart]
end

package "unzip" do
 action :install
end

bash "download_logstash_dashboard" do
not_if {Dir.exists?("/tmp/beats-dashboards-1.1.0")}
  user "root"
  cwd "/tmp"
  code <<-EOH
    wget --no-check-certificate https://download.elastic.co/beats/dashboards/beats-dashboards-1.1.0.zip -O /tmp/beats-dashboards-1.1.0.zip
    unzip beats-dashboards-1.1.0.zip
  EOH
end

template "/tmp/beats-dashboards-1.1.0/load.sh" do
 source "load.sh.erb"
 owner "root"
 group "root"
 mode "0755"
end

bash "install_logstash_dashboard" do
ignore_failure true
#not_if {Dir.exists?("/tmp/beats-dashboards-1.1.0")}
  user "root"
  cwd "/tmp"
  code <<-EOH
    (cd /tmp/beats-dashboards-1.1.0 && ./load.sh)
  EOH
end


 


